package backend.Json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class WeatherCityFactory {

    final static String PATH_TO_FILE = "src/resources/json/cities/city-list.json";

    static List<WeatherCity> getWeatherCities() {
        File file = new File(PATH_TO_FILE);
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<List<WeatherCity>> typeReference = new TypeReference<List<WeatherCity>>() {};

        try {
            return objectMapper.readValue(file, typeReference);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}