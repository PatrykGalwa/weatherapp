package backend.OpenApi;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class OpenWeatherApi {

    final static String API_BASE_URL = "https://openweathermap.org/data/2.5/weather?";
    final static String API_KEY = "f54a962dfa289c22d308c28878e75cb7";

    private final HttpClient httpClient;

    public OpenWeatherApi() {
        httpClient = HttpClients.createDefault();
    }

    public OpenWeatherApiResponse getResponses(Integer cityId) {

        try {
            URI uri = new URIBuilder(API_BASE_URL)
                    .addParameter("id", String.valueOf(cityId))
                    .addParameter("appid", API_KEY)
                    .addParameter("units", "metric")
                    .build();

            HttpGet httpGet = new HttpGet(uri);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            InputStream contentOfResponse = httpResponse.getEntity().getContent();
            Scanner s = new Scanner(contentOfResponse).useDelimiter("\\A");
            String jsonAsString = s.hasNext() ? s.next() : "";
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(jsonAsString, OpenWeatherApiResponse.class);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("cannot parse response");
    }
}