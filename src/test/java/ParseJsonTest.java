import backend.Json.WeatherCity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseJsonTest {

    @Test
    @DisplayName("should convert from json to weatherCity list")
    void shouldConvertFromJson() throws IOException {
        File file = new File("src/test/resources/json/test-city-list.json");
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<List<WeatherCity>> typeReference = new TypeReference<List<WeatherCity>>() {};
        List<WeatherCity> weatherCities = objectMapper.readValue(file, typeReference);

        assertEquals(2, weatherCities.size());

        WeatherCity firstElement = weatherCities.get(0);
        assertEquals(new Integer(707860), firstElement.getId());

        WeatherCity secondElement = weatherCities.get(1);
        assertEquals(new Integer(519188), secondElement.getId());
    }
}
